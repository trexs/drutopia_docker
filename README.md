# Drutopia Docker
A docker image for the Drutopia Distro. This process assumes 
[docker](https://docs.docker.com/engine/getstarted/) is installed on the host 
machine.  

## Current state of this Dockerfile
This Dockerfile is still under inital construction and is not yet reliable for
installing Drutopia or use as a development environment.

The Dockerfile is currently attempting to build up the environment from a base
Ubuntu 14.04 base image to the point it can support a `drupal site:install` 
from within a running container.

While drupal cli is installed in the environment, I am currently struggling to 
to get it to run `site:install without the following error:
```
root@89e9e751fb76:/opt/drutopia/myproject# drupal site:install
 [ERROR] Command "site:install", is not a valid command name.
```
However, when I run `drupal` the help page reports `site:install` as a possible 
command. I do not have previous drupal/php experience so I am having difficulty
tracing down a solution.

## Building a new drutopia image
```
docker build -t "drutopia:custom_image_tag" . 
```

## Login to a new drutopia docker container
```
docker run -p 127.0.0.1:3306:3306 -it drutopia bash
```

## Execute the following commands from the running container
```
drupal site:install
composer update
drush -y en features_ui drutopia_article drutopia_blog drutopia_comment drutopia_core drutopia_page drutopia_site drutopia_user
```
